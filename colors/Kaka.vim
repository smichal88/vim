"-----------------------------------------------------
" Vim color file
" Autor:	Damian Labuda <kaka2991@gmail.com>
"-----------------------------------------------------

" czcionka
set guifont=DejaVu\ Sans\ Mono\ 10
"set guifont="Andale Mono 8"
set gfn=DejaVu\ Sans\ Mono\ 10

set background=light

" kasuj poprzednie ustawienia
hi clear
if exists("syntax_on")
	syntax reset
endif

" ta linia musi byc nizej"}}}
let g:colors_name="Kaka"

" guibg = kolor tła ; guifg = kolor domyślnej czcionki
hi Normal	gui=none	guibg=#1f2030 guifg=#00ff00

" guifg = kolor stringów ; guibg = kolor tła
hi Constant	guifg=#b21818	guibg=bg

" Komentarze
hi Comment	gui=italic	guifg=#4444FF ctermfg=Blue

" ???
hi Identifier 	guifg=#40ffff

"switch, while, if, etc.
hi Statement	guifg=#de6f2a 	gui=none

" Preprocesor
"hi Special	guifg=#ff80ff
hi Special	guifg=Purple
"hi Special	guifg=#AA00AA

" void, int, double, etc.
hi Type		guifg=#00BF00 	gui=none

" Zaznaczanie, visual
hi Visual	ctermbg=Yellow	guifg=Yellow	guibg=RoyalBlue4 gui=none

" Kursor
hi Cursor	guibg=Yellow	guifg=Black	gui=bold

" Też string - ?
hi String	guifg=#b21818

hi ToDo		guibg=Red	guifg=Yellow

hi Search	ctermbg=Yellow	ctermfg=Red	guibg=Yellow guifg=Red

"hi LineNr       guibg=#1f3560   guifg=Yellow
"hi Folded	guibg=DarkGreen	guifg=Green
"hi Folded	guibg=#1f3560	guifg=RoyalBlue 

hi Folded	ctermbg=Cyan	ctermfg=Blue	guibg=#1a2c4f	guifg=RoyalBlue
hi FoldColumn	ctermbg=Blue	ctermfg=Yellow	guibg=#1a2c4f	guifg=RoyalBlue
hi LineNr	ctermbg=NONE	ctermfg=DarkYellow	guibg=#1f2030	guifg=#de6f2a

hi StatusLine	ctermfg=Green	guifg=Black	guibg=#00FF00 	gui=none
hi StatusLineNC	ctermfg=DarkGreen	guifg=#00ff00 guibg=Green

hi CursorLine	guibg=#1a2c4f
hi CursorColumn	guibg=#1a2c4f

hi VertSplit	guifg=DarkGreen guibg=Green
hi ModeMsg	guibg=#1a2c4f guifg=White 	gui=none

hi link PreProc	Special
hi link String Constant
hi MatchParen	term=reverse	ctermbg=Green	ctermfg=Yellow	guifg=Green	guibg=Blue

" DIFF
hi DiffChange   term=bold	ctermbg=Blue	guibg=RoyalBlue4
hi DiffAdd	term=bold	ctermbg=LightBlue	guibg=RoyalBlue
hi DiffText	term=reverse	cterm=bold	ctermbg=Yellow	gui=bold	guibg=RoyalBlue
hi DiffDelete	term=bold	ctermbg=Red	ctermfg=Yellow	guibg=Red	guifg=yellow

" taby (VIM7)
hi TabLineFill	ctermfg=DarkBlue	guifg=DarkGreen
hi TabLineSel  	ctermbg=Cyan	ctermfg=Yellow	guibg=Green	guifg=DarkGreen
hi TabLine  	ctermbg=Blue	ctermfg=White	guibg=DarkGreen	guifg=Green

" lista uzupeniania
hi Pmenu	ctermbg=DarkGreen ctermfg=Green guibg=DarkGreen guifg=Black
hi PmenuSel     ctermfg=Yellow ctermbg=Green guibg=Green guifg=White gui=bold
hi PmenuSbar	ctermbg=Green guibg=White
hi PmenuThumb	ctermfg=Green guifg=Blue
