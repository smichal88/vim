#!/bin/bash


if [ "$#" -eq 0 ] ; then


	git submodule foreach git pull 
	git submodule foreach git submodule update
	exit
fi



if [ "$1" -eq "--init" ] ;  then

	git submodule update --init 
	git submodule foreach git checkout maste 
	git submodule foreach git pull

	git submodule foreach git submodule update --init --recursive
	
else
	echo "You can run this script without parameters or with --init for init repository."
fi
