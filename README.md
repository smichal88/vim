My vim configuration


# Installation
To clone configuration use:

git clone --recursive https://smichal88@bitbucket.org/smichal88/vim.git


# Requirements

* ctags 
* vim with python support 
* clang compiler (for windows is included dll library)


# Keyboard shortcuts

## Global

	<Leader> \ 

### Windows navigation
		
	<Leader>j			switch to window below
	<Leader>k>			switch to window on top
	<Leader>h>			switch to window on right
	<Leader>l>			switch to window on left

	
	<Leader> v			vertical windows split

	:split				horizontally windows split

	<Leader> -			maximize verticali current window 
	<Leader> _			maximize horizontally current window
	<Leader> =			restore all splits

### Tab navigation

	<alt-Right>			next tab
	<alt-Left>			previous tab

	<ctrl-t>			open new tab
	<ctrl-q>			close current tab
	
### Line number
	
	<F2>				switch line number show
	<Leader> <F2>			switch relative line number show 

### Spell check
	
	<F7>				toggle spell checking

	<F7><Down>			next spell mistake (only in normal mode)
	<F7><Up>			previous spell mistake (only in normal mode)
	<F7>z				fix suggestions (only in normal mode)

### Move current line/selection up/down

	<ctrl-Up>			move line/selection one line up
	<ctrl-Down>			move line/selection one line down

### Make new fold
	
	<Leader>l			make new fold

### Open explorer (only in normal mode)

	<Leader>e			open folder explored
	<Leader>s			open folder explorer in split

### Fast undo (only on insert mode)

	<ctrl-z>			undo last change

### Update ctags

	<Leader>T			update ctags for current folder (only in normal mode)

### Toggle ctags windows
	
	<Leader> t			enable/disable tag list windows

### Completion

	<ctrl-Space>			completion (only in insert mode)


### Switch to paste mode

	<F11>				switch to paste mode

### Mouse switch

	<F12>				switch mouse to use in vim or terminal

### Disable last search highlight

	<Leader>/			disable last search highlight




## Python

	<F5>		run python code
	:PyLintAuto	Auto fix some pep8 error
	<Leader>b 	set breakpoint  
	<F3>		goto assignments 
	<Leader>d	goto definitions
	<F1>		show documentation
	<Leader>n	got to usage
	<Leader>r	rename command


### Jedi
		<Leader>r			rename variable 
		<F1>				display python help
		<Leader>d			go to original definition
		<F3>				go to definition



## C
### c_vim
		F9		compile and link
		Alt-F9		write buffer and compile
		Ctrl-F9		run executable 
		Shift-F9	set command line arguments
		Shift-F2	switch between source files and header files

Look at [https://github.com/vim-scripts/c.vim/raw/master/doc/c-hotkeys.pdf](https://github.com/vim-scripts/c.vim/raw/master/doc/c-hotkeys.pdf "c-hotkeys.pdf")
