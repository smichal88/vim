#!/bin/bash
if [[ -x /usr/bin/ctags ]]; then
	ctags -R --sort=yes --c++-kinds=+p --fields=+iaS --extra=+q --language-force=C++ -f qt4 /usr/include/qt4/
	# Get latest jquery 
	wget http://code.jquery.com/jquery-latest.js -O jquery.js
	ctags -f jquery jquery.js 
	rm jquery.js
fi
