" Syntax highlighting for jaro log
syn     match   warning         '.*\[WRN\].*'

syn     match   logNumber       '^\x\x '
syn     match   threadNumber    '0x\x\+ '
syn     match   module          '\u\+_*\u\+: '
syn     match   msgType         '\[\u\+\]'
syn     match   shuttDown       'Shutting down terminal'
syn     match   reactor         '\[\u\u\u\].*[Rr]eactor.*'
syn     match   qFuncInfo       '\[\u\u\u\] .*::.*(.*).*'
syn     match   allDataUpdate   'All remote data was updated'
syn     match   turnOff         'Powering off by PSU_PWROFF pin.'

syn     region  logString         start='"' end='"'

hi      link    warning         Error
hi      link    logNumber       Comment
hi      link    threadNumber    Constant
hi      link    module          Keyword
hi      link    shuttDown       Type
hi      link    logString       PreProc
hi      link    reactor         Type
hi      link    qFuncInfo       Todo
hi      link    allDataUpdate   Type
hi      link    msgType         Todo
hi      link    turnOff         Type
