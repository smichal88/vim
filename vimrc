"Global settings
"{
"
"Enabled Pathogen 
"{
runtime bundle/pathogen/autoload/pathogen.vim
call pathogen#infect()
call pathogen#helptags()
"}

"Enable syntax
syntax on

"Set color schema
set background=dark
colorscheme smichal

"Enable plug-in and indent for file detection
filetype plugin indent on

set backspace  	=indent,eol,start

"Set shell for Vim
set shell=/bin/bash

"Set spell Lang
set spelllang	=pl,en

"Set file encoding
set encoding		=utf-8
set fileencodings	=utf-8,cp1250

set nocompatible

"Display incomplete commands
set showcmd

"To get the File/Open dialog box to default to the current file's directory
set browsedir	=buffer

"Auto indent
set autoindent

"To paste code with/without indent
set pastetoggle	=<F11>

"Disable show line number
set nonumber

"Highlight matching brackets
set showmatch

"End of file about 5 lines on
set so		=5 

"Text after folding tabs
set foldtext	=FoldText()

"Highlight search word
set hlsearch

"Always show cursor
set ruler

"Some menu list config
set wildmode	=longest,list 

"Set 256 colors in terminal
if &term =~ "xterm"
	set t_Co =256
endif

"Enable per-directory .vimrc files
set exrc

"Disable unsafe commands in local .vimrc files
set secure

"When you have not permission  
cmap w!! w !sudo tee % > /dev/null

"Set default latex compiling format
let g:Tex_DefaultTargetFormat='pdf'
let g:Tex_MultipleCompileFormats='pdf, aux'

"Add local tags to tags
set tags +=./.tags;
set tags +=~/.vim/tags/qt4
set tags +=~/.vim/tags/jquery
set tags +=~/.vim/tags/CV_python

if ! exists('g:TagHighlightSettings')
	let g:TagHighlightSettings = {}
endif
let g:TagHighlightSettings['TagFileName'] = '.tags'

"}

"Clang completer
"{

if has("win32") || has("win64")
	" Enable local clang library
	let g:clang_library_path = $HOME . "\\vimfiles\\lib"
endif
"}

"Hex helper
"{
"Ex command for toggling hex mode - define mapping if desired
command -bar Hexmode call ToggleHex()

"Helper function to toggle hex mode
function ToggleHex()
  " hex mode should be considered a read-only operation
  " save values for modified and read-only for restoration later,
  " and clear the read-only flag for now
  let l:modified=&mod
  let l:oldreadonly=&readonly
  let &readonly=0
  let l:oldmodifiable=&modifiable
  let &modifiable=1
  if !exists("b:editHex") || !b:editHex
    " save old options
    let b:oldft=&ft
    let b:oldbin=&bin
    " set new options
    setlocal binary " make sure it overrides any textwidth, etc.
    let &ft="xxd"
    " set status
    let b:editHex=1
    " switch to hex editor
    %!xxd
  else
    " restore old options
    let &ft=b:oldft
    if !b:oldbin
      setlocal nobinary
    endif
    " set status
    let b:editHex=0
    " return to normal editing
    %!xxd -r
  endif
  " restore values for modified and read only state
  let &mod=l:modified
  let &readonly=l:oldreadonly
  let &modifiable=l:oldmodifiable
endfunction

"}

"JavaScript settings
"{

autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS

"}

"Python settings
"{

let g:pymode_rope = 0
let g:jedi#use_tabs_not_buffers = 1
let g:pymode_rope_guess_project=0
let g:pymode_lint_checker = "pyflakes,pep8,mccabe"

"}

"Project settings
"{
 let g:project_use_nerdtree = 1
 set rtp+=~/.vim/bundle/project/
 call project#rc("~/Programowanie")

"To add project add on root directory .vimrc file
" with 'call project#rc(getcwd())'
" and 'Project 'dir_name_of_project''

"}

"Key map
"{

"Compile PovRay code
autocmd FileType pov map <F5> :!povray +W640 +H480 +X +P %<CR>

"Easy window navigation
"{
nnoremap <Leader>j 	:wincmd j<CR>
nnoremap <Leader>k 	:wincmd k<CR>
nnoremap <Leader>h	:wincmd h<CR>
nnoremap <Leader>l	:wincmd l<CR>
"}

"Vertical split
"{
nnoremap <Leader>v 	:vsplit<CR>
inoremap <Leader>v 	<ESC><CR>:vsplit<CR>
"}

"Split size
"{
nnoremap <Leader>-	<C-w><BAR>
inoremap <Leader>-	<C-w><BAR>

nnoremap <Leader>_	<C-w>_
inoremap <Leader>_	<C-w>_

nnoremap <Leader>=	<C-w>=
inoremap <Leader>=	<C-w>=
"}

"Line number
"{
"Turn on/off line number
map	<F2> 		:set nu!<CR>
imap	<F2> 		<ESC><CR>:set nu!<CR>
"Turn on/off relative line number
map	<Leader><F2>	:set rnu!<CR>
imap	<Leader><F2>	<ESC><CR>:set rnu!<CR>
"}

"Disabled highlight current search
"{
map	<Leader>/	:nohlsearch<CR>
imap	<Leader>/	<ESC>:nohlsearch<CR>
"}

"Spell check
"{
map	<F7> 		<ESC>:setlocal spell!<CR>
imap	<F7> 		<ESC>:setlocal spell!<CR>i<right>
map	<F7><Down>	]s
map	<F7><Up>	[s
map	<F7>z		z=
"}

"Tab navigation
"{
map	<A-Right>	:tabnext<CR>
nmap	<A-Right>	:tabnext<CR>
imap	<A-Right>	<Esc>:tabnext<CR>a

map	<A-Left>	:tabprevious<CR>
nmap	<A-Left>	:tabprevious<CR>
imap	<A-Left>	<Esc>:tabprevious<CR>a

nmap	<C-t>		:tabnew<CR>
imap	<C-t>		<Esc>:tabnew<CR>

nmap	<C-q>		:tabclose<CR>
imap	<C-q>		<Esc>:tabclose<CR>
"}

"Move current line/selection up/down
"{
map		<C-Down>	:m+<CR>==
map		<C-Up>		:m-2<CR>==
vmap		<C-Up>		:m-2<CR>gv
vmap		<C-Down>	:m'>+<CR>gv
inoremap 	<C-Down>	<Esc>:m+<CR>==gi
inoremap 	<C-Up>		<Esc>:m-2<CR>==gi
"}

"Html file
"{
autocmd FileType html call SetHtmlKeys()
autocmd FileType css call SetHtmlKeys()
autocmd FileType htmldjango call SetHtmlKeys()
autocmd FileType xml call SetHtmlKeys()
autocmd FileType xslt call SetHtmlKeys()


function SetHtmlKeys()
	"Run zen-coding (emmet)
	imap 	<C-Space>	<C-y>,
	imap	<F5>n		<C-y>n
	imap	<F5>N		<C-y>N
	imap	<F5>i		<C-y>i
	imap	<F5>k		<C-y>k
	imap	<F5>/		<C-y>/
	imap	<F5>a		<C-y>a

endfunction

"Folding for html tags
nmap	<Leader>z	zfit

"}

" Python 
"{
let g:jedi#documentation_command = "<F1>"
let g:jedi#goto_assignments_command = "<F3>"
let g:pymode_run_key = '<F5>'
"}

"Update highlight ctag
"{
:map		<Leader>T	:UpdateTypesFile<CR>
:map		<Leader>t	:TlistToggle<CR>
"}

"Open file explorer
"{
map	<Leader>e :Explore<cr>
map	<Leader>s :Sexplore<cr>
"}

"Select all.
"{
map	<c-a> ggVG
imap	<c-a> <ESC>ggVG
"}

"Undo in insert mode.
"{
imap <c-z> <c-o>u
"}


"OmiFunc to ctr + space
"{
inoremap <expr> <C-Space> pumvisible() \|\| &omnifunc == '' ?
\ "\<lt>C-n>" :
\ "\<lt>C-x>\<lt>C-o><c-r>=pumvisible() ?" .
\ "\"\\<lt>c-n>\\<lt>c-p>\\<lt>c-n>\" :" .
\ "\" \\<lt>bs>\\<lt>C-n>\"\<CR>"
imap <C-@> <C-Space>
"}

"}

" vim: set foldmarker={,} foldlevel=0 foldmethod=marker spell:
